main: main.o lib.o dict.o
	ld -o main main.o lib.o dict.o
main.o: main.asm colon.inc
	nasm -f elf64 main.asm
dict.o: dict.asm
	nasm -f elf64 dict.asm
lib.o: lib.asm 
	nasm -f elf64 lib.asm
	.PHONY : clean	
clean : 
	rm -rf *.o
	rm -rf main
