%include "words.inc"

%define size 256

section .data

start_message: db "Write the key",10,0

found_message: db "Word's found: ",0

not_found_message: db "Word isn't found",10,0

too_big_message: db "Word is too big",0

section .text

global _start:

extern read_word, find_word, print_string, exit,print_newline, string_length

_start:
	mov rdi, start_message
	call print_string
	push rbp
	sub rsp,size
	mov rbp,rsp
	mov rsi, size-1
	mov rdi, rsp
	call read_word
	cmp rax,0
	je .str_too_big
	mov rdi,rax
	mov rsi, next
	call find_word
	test rax,rax
	jz .not_found
	mov rsp,rbp
	pop rbp
	add rax,8
	mov r10,rax
	mov rdi,rax
	call string_length
	mov r9,rax
	inc r9
	mov rdi, found_message
	call print_string
	add r10,r9
	mov rdi,r10
	call print_string
	call print_newline
	mov rdi,0
	call exit
.str_too_big:
	mov rsp,rbp
	pop rbp
	mov rdi, too_big_message
	call print_string_err
	mov rdi,1
	call exit
.not_found:
	mov rsp,rbp
	pop rbp
	mov rdi, not_found_message
	call print_string_err
	mov rdi,1
	call exit
print_string_err:
	call string_length
	mov rdx,rax
	mov rax,1
	mov rsi,rdi
	mov rdi,2
	syscall
	ret
